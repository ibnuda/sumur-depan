module Data.Tagihan.Pelanggan exposing (..)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (decode, required)


decoder : Decoder Pelanggan
decoder =
    decode Pelanggan
        |> required "nomor_pelanggan" Decode.int
        |> required "nama_pelanggan" Decode.string
        |> required "nomor_telepon" Decode.string
        |> required "alamat" Decode.string
        |> required "wilayah" Decode.string


type alias Pelanggan =
    { nomorPelanggan : Int
    , namaPelanggan : String
    , nomorTelepon : String
    , alamat : String
    , wilayah : String
    }
