module Data.Pelanggan exposing (..)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (custom, decode, optional, required)


type alias DataPelanggan =
    { namaPelanggan : String
    , nomorTelepon : String
    , nomorMeteran : String
    , alamat : String
    , wilayah : String
    }


dataPelangganDecoder : Decoder DataPelanggan
dataPelangganDecoder =
    decode DataPelanggan
        |> required "nama_pelanggan" Decode.string
        |> required "nomor_telepon" Decode.string
        |> required "nomor_meteran" Decode.string
        |> required "alamat" Decode.string
        |> required "wilayah" Decode.string


daftarDataPelangganDecoder : Decoder (List DataPelanggan)
daftarDataPelangganDecoder =
    Decode.list dataPelangganDecoder
