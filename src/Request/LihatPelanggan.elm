module Request.LihatPelanggan exposing (..)

import Data.AuthToken exposing (AuthToken, withAuthorization)
import Data.Pelanggan as Pelanggan exposing (..)
import Data.DetailPelanggan as DetailPelanggan exposing (..)
import Http
import HttpBuilder exposing (RequestBuilder, withBody, withExpect, withQueryParams)
import Request.Bantuan exposing (apiUrl)


getDataPelanggan : Maybe AuthToken -> String -> Http.Request DataPelanggan
getDataPelanggan mtoken notelp =
    let
        ekspektasi =
            Pelanggan.dataPelangganDecoder
                |> Http.expectJson
    in
    apiUrl ("/petugas/air/" ++ notelp)
        |> HttpBuilder.get
        |> HttpBuilder.withExpect ekspektasi
        |> withAuthorization mtoken
        |> HttpBuilder.toRequest


getDaftarDataPelanggan : Maybe AuthToken -> Http.Request (List DataPelanggan)
getDaftarDataPelanggan mtoken =
    let
        ekspektasi =
            Pelanggan.daftarDataPelangganDecoder
                |> Http.expectJson
    in
    apiUrl "/pelanggan"
        |> HttpBuilder.get
        |> HttpBuilder.withExpect ekspektasi
        |> withAuthorization mtoken
        |> HttpBuilder.toRequest

getDetailPelanggan : Maybe AuthToken -> String -> Http.Request DetailPelanggan
getDetailPelanggan mtoken nomormeteran =
    let
        ekspektasi =
            DetailPelanggan.detailPelangganDecoder
                |> Http.expectJson
    in
    apiUrl ("/pelanggan/" ++ nomormeteran)
        |> HttpBuilder.get
        |> HttpBuilder.withExpect ekspektasi
        |> withAuthorization mtoken
        |> HttpBuilder.toRequest