module Laman.DaftarDataPelangganBray exposing (..)

import Data.Pelanggan as Pelanggan exposing (..)
import Data.Sesi as Sesi exposing (..)
import Html exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, decodeString, field, list, string)
import Laman.GagalMuat as GagalMuat exposing (..)
import Request.LihatPelanggan as LihatPelanggan exposing (..)
import Route
import Task exposing (Task)
import Util exposing ((=>))
import Views.LamanLaman as LamanLaman exposing (..)


type alias Model =
    { galatPelanggan : String
    , daftarDataPelanggan : List DataPelanggan
    }


init : Sesi -> Task LamanGagalMuat Model
init sesi =
    let
        mtoken =
            Maybe.map .token sesi.pengguna

        muattagihan =
            LihatPelanggan.getDaftarDataPelanggan mtoken
                |> Http.toTask

        tangangagal _ =
            lamanGagalDimuat LamanLaman.Lain "Gagal memuat tagihan. Mungkin tidak ada."
    in
    Task.map (Model "") muattagihan
        |> Task.mapError tangangagal


view : Sesi -> Model -> Html Msg
view sesi model =
    table []
        [ thead []
            [ tr []
                [ th [] [ text "Nama " ]
                , th [] [ text "Nomor Telepon" ]
                , th [] [ text "Nomor meteran" ]
                , th [] [ text "Alamat" ]
                , th [] [ text "Wilayah" ]
                ]
            ]
        , tbody [] (List.map viewDaftarDataPelangganBray model.daftarDataPelanggan)
        ]


viewDaftarDataPelangganBray : DataPelanggan -> Html msg
viewDaftarDataPelangganBray pelanggan =
    tr []
        [ td [] [ text pelanggan.namaPelanggan ]
        , td [] [ text pelanggan.nomorTelepon ]
        , td []
            [ a [ Route.href (Route.DaftarTagihan pelanggan.nomorMeteran) ]
                [ text pelanggan.nomorMeteran ]
            ]
        , td [] [ text pelanggan.alamat ]
        , td [] [ text pelanggan.wilayah ]
        ]

type Msg
    = NoOp
    | DaftarDataPelangganTerunduh (Result Http.Error (List DataPelanggan))


update : Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case ( sesi.pengguna, msg ) of
        ( Nothing, _ ) ->
            model
                => Cmd.batch
                    [ Route.modifikasiUrl Route.Beranda
                    ]

        ( Just x, NoOp ) ->
            model
                => Cmd.none

        ( Just x, DaftarDataPelangganTerunduh (Ok dp) ) ->
            { model | daftarDataPelanggan = dp }
                => Cmd.none

        ( Just x, DaftarDataPelangganTerunduh (Err r) ) ->
            let
                pesangalat =
                    case r of
                        Http.BadStatus r ->
                            r.body
                                |> Decode.decodeString Decode.string
                                |> Result.withDefault "bad code."

                        _ ->
                            "tidak boleh lihat tagihan ini."
            in
            { model | galatPelanggan = pesangalat }
                => Cmd.batch
                    [ Route.modifikasiUrl Route.Beranda
                    ]
