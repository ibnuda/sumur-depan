module Laman.GagalMuat exposing (..)

{-| The page that renders when there was an error trying to load another page,
for example a Page Not Found error.
It includes a photo I took of a painting on a building in San Francisco,
of a giant walrus exploding the golden gate bridge with laser beams. Pew pew!
-}

import Data.Sesi exposing (Sesi)
import Html exposing (Html, div, h1, img, main_, p, text)
import Html.Attributes exposing (alt, class, id, tabindex)
import Views.LamanLaman exposing (LamanAktif)


-- MODEL --


type LamanGagalMuat
    = LamanGagalMuat Model


type alias Model =
    { lamanAktif : LamanAktif
    , pesanGalat : String
    }


lamanGagalDimuat : LamanAktif -> String -> LamanGagalMuat
lamanGagalDimuat activePage errorMessage =
    LamanGagalMuat
        { lamanAktif = activePage
        , pesanGalat = errorMessage
        }



-- VIEW --


view : Sesi -> LamanGagalMuat -> Html msg
view session (LamanGagalMuat model) =
    main_ [ id "content", tabindex -1 ]
        [ h1 [] [ text "Kok bisa gagal, ya?" ]
        , div []
            [ p [] [ text model.pesanGalat ] ]
        ]
