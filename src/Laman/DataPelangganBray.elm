module Laman.DataPelangganBray exposing (..)

import Data.DetailPelanggan as DetailPelanggan exposing (..)
import Data.Sesi as Sesi exposing (..)
import Html exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, decodeString, field, list, string)
import Laman.GagalMuat as GagalMuat exposing (..)
import Request.LihatPelanggan as LihatPelanggan exposing (..)
import Route
import Task exposing (Task)
import Util exposing ((=>))
import Views.LamanLaman as LamanLaman exposing (..)


type alias Model =
    { galatPelanggan : String
    , dataPelanggan : DetailPelanggan
    }


init : Sesi -> String -> Task LamanGagalMuat Model
init sesi notelp =
    let
        mtoken =
            Maybe.map .token sesi.pengguna

        muattagihan =
            LihatPelanggan.getDetailPelanggan mtoken notelp
                |> Http.toTask

        tangangagal _ =
            lamanGagalDimuat LamanLaman.Lain "Gagal memuat detail pelanggan."
    in
    Task.map (Model "") muattagihan
        |> Task.mapError tangangagal


view : Sesi -> Model -> Html Msg
view sesi model =
    div []
        [ div []
            [ div []
                [ div []
                    [ viewDataPelangganBray model.dataPelanggan
                    ]
                ]
            , hr [] []
            ]
        ]


viewDataPelangganBray : DetailPelanggan -> Html msg
viewDataPelangganBray pelanggan =
    div []
        [ p [] [ text <| "Nama Pelanggan: " ++ pelanggan.namaPelanggan ]
        , p [] [ text <| "Nomor Telepon: " ++ pelanggan.nomorTelepon ]
        , p [] [ text <| "Nomor Meteran: " ++ pelanggan.nomorMeteran ]
        , p [] [ text <| "Alamat: " ++ pelanggan.alamat ]
        , p [] [ text <| "Tanggal Daftar" ++ pelanggan.tanggalDaftar ]
        ]


type Msg
    = NoOp
    | DataPelangganTerunduh (Result Http.Error DetailPelanggan)


update : Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case ( sesi.pengguna, msg ) of
        ( Nothing, _ ) ->
            model
                => Cmd.batch
                    [ Route.modifikasiUrl Route.Beranda
                    ]

        ( Just x, NoOp ) ->
            model
                => Cmd.none

        ( Just x, DataPelangganTerunduh (Ok dp) ) ->
            { model | dataPelanggan = dp }
                => Cmd.none

        ( Just x, DataPelangganTerunduh (Err r) ) ->
            let
                pesangalat =
                    case r of
                        Http.BadStatus r ->
                            r.body
                                |> Decode.decodeString Decode.string
                                |> Result.withDefault "bad code."

                        _ ->
                            "tidak boleh lihat tagihan ini."
            in
            { model | galatPelanggan = pesangalat }
                => Cmd.batch
                    [ Route.modifikasiUrl Route.Beranda
                    ]
