module Laman.Beranda exposing (..)

import Data.Sesi exposing (Sesi)
import Html exposing (Html, a, div, h1, img, main_, text)
import Html.Attributes exposing (alt, class, href, id, src, tabindex)


type alias Model =
    { nama : String
    }


modelAwal : Model
modelAwal =
    { nama = "beranda bray." }


initBeranda : Sesi -> Model
initBeranda sesi =
    { nama = "" }


view : Sesi -> Html msg
view sesi =
    main_ [ id "content", tabindex -1 ]
        [ h1 [] [ text "Not Found" ]
        , div []
            [ a [ href "/#/tagihan/jaran/2018/5" ] [ text "sini" ]
            ]
        ]
