module Laman.DaftarTagihanBray exposing (..)

import Data.Sesi as Sesi exposing (..)
import Data.Tagihan as Tagihan exposing (..)
import Html exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, decodeString, field, list, string)
import Laman.GagalMuat as GagalMuat exposing (..)
import Laman.TagihanBray exposing (viewTagihanBray)
import Request.TagihanPelanggan as TagihanPelanggan exposing (..)
import Task exposing (Task)
import Util exposing ((=>))
import Views.LamanLaman as LamanLaman exposing (..)


type alias Model =
    { galatMuat : String
    , tagihan : List Tagihan
    }


init : Sesi -> String -> Task LamanGagalMuat Model
init sesi nomet =
    let
        mtoken =
            Maybe.map .token sesi.pengguna

        muattagihan =
            TagihanPelanggan.getTagihanNomerMeteran mtoken nomet
                |> Http.toTask

        tangangagal _ =
            lamanGagalDimuat LamanLaman.Lain "Gagal memuat tagihan. Mungkin tidak ada."
    in
    Task.map (Model "") muattagihan
        |> Task.mapError tangangagal


view : Sesi -> Model -> Html Msg
view sesi model =
    div []
        [ div []
            [ div [] (List.map viewTagihanBray model.tagihan)
            ]
        ]


type Msg
    = NoOp
    | DaftarTagihanTerunduh (Result Http.Error (List Tagihan))


update : Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case msg of
        NoOp ->
            model => Cmd.none

        DaftarTagihanTerunduh (Ok tagihan) ->
            { model | tagihan = tagihan } => Cmd.none

        DaftarTagihanTerunduh (Err galat) ->
            let
                pesangalat =
                    case galat of
                        Http.BadStatus r ->
                            r.body
                                |> Decode.decodeString Decode.string
                                |> Result.withDefault "bad code."

                        _ ->
                            "tidak boleh lihat tagihan ini."
            in
            { model | galatMuat = pesangalat }
                => Cmd.none
