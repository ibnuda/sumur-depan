module Laman.Masuk exposing (..)

import Data.Pengguna exposing (Pengguna)
import Data.Sesi exposing (Sesi)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (decode, optional)
import Request.Pengguna exposing (simpanSesi)
import Route exposing (Rute)
import Util exposing ((=>))
import Validate exposing (Validator, ifBlank, validate)
import Views.Borang as Borang


type alias Model =
    { nomorTelp : String
    , password : String
    , galat : List Galat
    }


modelAwal : Model
modelAwal =
    { nomorTelp = ""
    , password = ""
    , galat = []
    }


view : Sesi -> Model -> Html Msg
view sesi model =
    div [ class "pure-g" ]
        [ div [ class "pure-u-1 pure-u-md-1-3" ] []
        , div [ class "pure-u-1 pure-u-md-1-3" ]
            [ div [ class "header-medium" ]
                [ div []
                    [ Borang.viewGalat model.galat
                    , viewBorang
                    ]
                ]
            ]
        , div [ class "pure-u-1 pure-u-md-1-3" ] []
        ]


viewBorang : Html Msg
viewBorang =
    Html.form [ class "pure-form pure-form-stacked", onSubmit SubmitBorang ]
        [ Borang.input
            [ class "pure-input-1"
            , placeholder "Nomor Telepon"
            , onInput SetNomorTelp
            ]
            []
        , Borang.password
            [ class "pure-input-1"
            , placeholder "Password"
            , onInput SetPassword
            ]
            []
        , button [ class "pure-button pure-success" ]
            [ text "Masuk" ]
        ]


type Msg
    = SubmitBorang
    | SetNomorTelp String
    | SetPassword String
    | MasukSelesai (Result Http.Error Pengguna)


type ExternalMsg
    = NoOp
    | SetPengguna Pengguna


update : Sesi -> Msg -> Model -> ( ( Model, Cmd Msg ), ExternalMsg )
update sesi msg model =
    case sesi.pengguna of
        Just _ ->
            model
                => Cmd.batch [ Route.modifikasiUrl Route.Beranda ]
                => NoOp

        Nothing ->
            case msg of
                SetNomorTelp t ->
                    { model | nomorTelp = t } => Cmd.none => NoOp

                SetPassword p ->
                    { model | password = p } => Cmd.none => NoOp

                SubmitBorang ->
                    case validate modelValidator model of
                        [] ->
                            { model | galat = [] }
                                => Http.send MasukSelesai (Request.Pengguna.masuk model)
                                => NoOp

                        galat ->
                            { model | galat = galat } => Cmd.none => NoOp

                MasukSelesai (Err galat) ->
                    let
                        pesangalat =
                            case galat of
                                Http.BadStatus r ->
                                    r.body
                                        |> Decode.decodeString (Decode.field "errors" errorsDecoder)
                                        |> Result.withDefault []

                                _ ->
                                    [ "tak bisa login." ]
                    in
                    { model | galat = List.map (\perr -> Borang => perr) pesangalat }
                        => Cmd.none
                        => NoOp

                MasukSelesai (Ok p) ->
                    model
                        => Cmd.batch [ simpanSesi p, Route.modifikasiUrl Route.DaftarPengguna ]
                        => SetPengguna p


type Field
    = Borang
    | NomorTelepon
    | Password


type alias Galat =
    ( Field, String )


modelValidator : Validator Galat Model
modelValidator =
    Validate.all
        [ ifBlank .nomorTelp (NomorTelepon => "Nomor telepon tidak boleh kosong")
        , ifBlank .password (Password => "Password tidak boleh kosong.")
        ]


errorsDecoder : Decoder (List String)
errorsDecoder =
    decode (\notelpAtauPassword notelp password -> List.concat [ notelpAtauPassword, notelp, password ])
        |> optionalError "notelp atau password"
        |> optionalError "notelp"
        |> optionalError "password"


optionalError : String -> Decoder (List String -> a) -> Decoder a
optionalError fieldName =
    let
        errorToString errorMessage =
            String.join " " [ fieldName, errorMessage ]
    in
    optional fieldName (Decode.list (Decode.map errorToString string)) []
