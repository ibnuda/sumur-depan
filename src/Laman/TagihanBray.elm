module Laman.TagihanBray exposing (..)

import Data.Sesi as Sesi exposing (..)
import Data.Tagihan as Tagihan exposing (..)
import Data.Tagihan.Pelanggan as Pelanggan exposing (..)
import Data.Tagihan.Tarif as Tarif exposing (..)
import Html exposing (..)
import Html.Attributes exposing (attribute, class, disabled, href, id, placeholder)
import Http
import Json.Decode as Decode exposing (Decoder, decodeString, field, list, string)
import Laman.GagalMuat as GagalMuat exposing (..)
import Request.TagihanPelanggan as TagihanPelanggan exposing (..)
import Task exposing (Task)
import Util exposing ((=>))
import Views.LamanLaman as LamanLaman exposing (..)


type alias Model =
    { galatMuat : String
    , tagihan : Tagihan
    }


init : Sesi -> String -> Int -> Int -> Task LamanGagalMuat Model
init sesi nomet tahun bulan =
    let
        mtoken =
            Maybe.map .token sesi.pengguna

        muattagihan =
            TagihanPelanggan.getSpesifik mtoken nomet tahun bulan
                |> Http.toTask

        tangangagal _ =
            lamanGagalDimuat LamanLaman.Lain "Gagal memuat tagihan. Mungkin tidak ada."
    in
    Task.map (Model "") muattagihan
        |> Task.mapError tangangagal


view : Sesi -> Model -> Html Msg
view sesi model =
    div []
        [ div []
            [ div []
                [ div []
                    [ viewTagihanBray model.tagihan
                    ]
                ]
            , hr [] []
            ]
        ]


viewTagihanBray : Tagihan -> Html msg
viewTagihanBray tagihan =
    div []
        [ h1 [] [ text "Tagihan Minum" ]
        , p [] [ text tagihan.nomorMeteran ]
        , p [] [ text <| "Tahun : " ++ toString tagihan.tahun ++ " Bulan " ++ toString tagihan.bulan ]
        , p [] [ text <| "Tanggal Bayar : " ++ tagihan.tanggalBayar ]
        , p [] [ text <| "Minum: " ++ toString (tagihan.minumSekarang - tagihan.minumLalu) ]
        , viewPelanggan tagihan.pengguna
        , viewTarif tagihan.tarif
        ]


terjepit : Int -> TarifItem -> Int
terjepit minum tarifitem =
    let
        terjepit =
            clamp tarifitem.mulai tarifitem.sampai minum
    in
    minum - terjepit


viewPelanggan : Pelanggan -> Html msg
viewPelanggan pelanggan =
    div []
        [ p [] [ text <| "Nama Pelanggan: " ++ pelanggan.namaPelanggan ]
        , p [] [ text <| "Nomor Telepon: " ++ pelanggan.nomorTelepon ]
        , p [] [ text <| "Nomor Pelanggan: " ++ toString pelanggan.nomorPelanggan ]
        , p [] [ text <| "Alamat: " ++ pelanggan.alamat ]
        , p [] [ text <| "Wilayah: " ++ pelanggan.wilayah ]
        ]


viewTarif : Tarif -> Html msg
viewTarif tarif =
    div []
        [ p [] [ text <| "Biaya Beban :" ++ toString tarif.biayaBeban ]
        , div [ class "pure-g" ] <| List.map viewTarifItem tarif.satuan
        ]


viewTarifItem : TarifItem -> Html msg
viewTarifItem tarifitem =
    div [ class "pure-u-1 pure-u-md-1-3" ]
        [ p [] [ text <| "Mulai" ++ toString tarifitem.mulai ]
        , p [] [ text <| "Sampai :" ++ toString tarifitem.sampai ]
        , p [] [ text <| "Harga " ++ toString tarifitem.harga ]
        ]


type Msg
    = NoOp
    | TagihanTerunduh (Result Http.Error Tagihan)


update : Sesi -> Msg -> Model -> ( Model, Cmd Msg )
update sesi msg model =
    case msg of
        NoOp ->
            model => Cmd.none

        TagihanTerunduh (Ok tagihan) ->
            { model | tagihan = tagihan } => Cmd.none

        TagihanTerunduh (Err galat) ->
            let
                pesangalat =
                    case galat of
                        Http.BadStatus r ->
                            r.body
                                |> Decode.decodeString Decode.string
                                |> Result.withDefault "bad code."

                        _ ->
                            "tidak boleh lihat tagihan ini."
            in
            { model | galatMuat = pesangalat }
                => Cmd.none
