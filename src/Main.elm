module Main exposing (main)

import Data.Pengguna as Pengguna exposing (Pengguna)
import Data.Sesi exposing (Sesi)
import Html exposing (..)
import Json.Decode as Decode exposing (Value)
import Laman.Beranda as Beranda
import Laman.DaftarDataPelangganBray as DaftarDataPelangganBray exposing (..)
import Laman.DaftarTagihanBray as DaftarTagihanBray exposing (..)
import Laman.DataPelangganBray as DataPelangganBray exposing (..)
import Laman.GagalMuat as GagalMuat exposing (LamanGagalMuat)
import Laman.Masuk as Masuk
import Laman.TagihanBray as TagihanBray exposing (..)
import Navigation exposing (Location)
import Ports
import Route exposing (Rute)
import Task exposing (Task)
import Util exposing ((=>))
import Views.LamanLaman as LamanLaman exposing (LamanAktif)


type HalamanBrowser
    = Kosong
    | Beranda Beranda.Model
    | GagalDimuat LamanGagalMuat
    | LihatPelanggan DataPelangganBray.Model
    | LihatDaftarPelanggan DaftarDataPelangganBray.Model
    | LihatTagihan TagihanBray.Model
    | LihatDaftarTagihan DaftarTagihanBray.Model
    | Masuk Masuk.Model


type StatusLaman
    = Termuat HalamanBrowser
    | PindahDari HalamanBrowser


type alias Model =
    { sesi : Sesi
    , statusLaman : StatusLaman
    }


decodePenggunaDariJson : Value -> Maybe Pengguna
decodePenggunaDariJson json =
    json
        |> Decode.decodeValue Decode.string
        |> Result.toMaybe
        |> Maybe.andThen (Decode.decodeString Pengguna.decoder >> Result.toMaybe)


lamanAwal : HalamanBrowser
lamanAwal =
    Kosong


init : Value -> Location -> ( Model, Cmd Msg )
init v lokasi =
    setRute (Route.dariLokasi lokasi)
        { statusLaman = Termuat lamanAwal
        , sesi = { pengguna = decodePenggunaDariJson v }
        }


view : Model -> Html Msg
view model =
    case model.statusLaman of
        Termuat laman ->
            viewLaman model.sesi False laman

        PindahDari laman ->
            viewLaman model.sesi True laman


viewLaman : Sesi -> Bool -> HalamanBrowser -> Html Msg
viewLaman sesi sedangmemuat laman =
    let
        frame =
            LamanLaman.bingkai sedangmemuat sesi.pengguna
    in
    case laman of
        Kosong ->
            Html.text "Sike Nigga! You thought!" |> frame LamanLaman.Lain

        Beranda submodel ->
            Beranda.view sesi
                |> frame LamanLaman.Beranda

        Masuk submodel ->
            Masuk.view sesi submodel
                |> frame LamanLaman.Lain
                |> Html.map MasukMsg

        LihatDaftarPelanggan submodel ->
            DaftarDataPelangganBray.view sesi submodel
                |> frame LamanLaman.AktifDaftarPelanggan
                |> Html.map DaftarDataPelangganMsg

        LihatPelanggan submodel ->
            DataPelangganBray.view sesi submodel
                |> frame LamanLaman.AktifPelangganSpesifik
                |> Html.map DataPelangganMsg

        LihatDaftarTagihan submodel ->
            DaftarTagihanBray.view sesi submodel
                |> frame LamanLaman.AktifTagihanSpesifik
                |> Html.map DaftarTagihanMsg

        LihatTagihan submodel ->
            TagihanBray.view sesi submodel
                |> frame LamanLaman.Lain
                |> Html.map TagihanMsg

        GagalDimuat submodel ->
            GagalMuat.view sesi submodel
                |> frame LamanLaman.Lain


type Msg
    = MasukMsg Masuk.Msg
    | SetRoute (Maybe Rute)
    | BerandaTermuat (Result LamanGagalMuat Beranda.Model)
    | DaftarDataPelangganTermuat (Result LamanGagalMuat DaftarDataPelangganBray.Model)
    | DataPelangganTermuat (Result LamanGagalMuat DataPelangganBray.Model)
    | DaftarTagihanTermuat (Result LamanGagalMuat DaftarTagihanBray.Model)
    | TagihanTermuat (Result LamanGagalMuat TagihanBray.Model)
    | SetPengguna (Maybe Pengguna)
    | DaftarDataPelangganMsg DaftarDataPelangganBray.Msg
    | DataPelangganMsg DataPelangganBray.Msg
    | TagihanMsg TagihanBray.Msg
    | DaftarTagihanMsg DaftarTagihanBray.Msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ subscriptionsLaman (getLaman model.statusLaman)
        , Sub.map SetPengguna ubahSesi
        ]


ubahSesi : Sub (Maybe Pengguna)
ubahSesi =
    Ports.onSessionChange (Decode.decodeValue Pengguna.decoder >> Result.toMaybe)


getLaman : StatusLaman -> HalamanBrowser
getLaman statuslaman =
    case statuslaman of
        Termuat laman ->
            laman

        PindahDari laman ->
            laman


subscriptionsLaman : HalamanBrowser -> Sub msg
subscriptionsLaman laman =
    case laman of
        Kosong ->
            Sub.none

        Beranda _ ->
            Sub.none

        Masuk _ ->
            Sub.none

        LihatDaftarPelanggan _ ->
            Sub.none

        LihatPelanggan _ ->
            Sub.none

        LihatDaftarTagihan _ ->
            Sub.none

        LihatTagihan _ ->
            Sub.none

        GagalDimuat _ ->
            Sub.none


setRute : Maybe Rute -> Model -> ( Model, Cmd Msg )
setRute mrute model =
    let
        transisi tomsg task =
            { model | statusLaman = PindahDari (getLaman model.statusLaman) }
                => Task.attempt tomsg task

        digalatkan =
            lamanJadiGalat model
    in
    case ( model.sesi.pengguna, mrute ) of
        ( Just _, Just Route.Beranda ) ->
            { model | statusLaman = Termuat (Beranda Beranda.modelAwal) } => Cmd.none

        ( Just _, Just Route.DaftarPengguna ) ->
            transisi DaftarDataPelangganTermuat (DaftarDataPelangganBray.init model.sesi)

        ( Just _, Just (Route.PenggunaSpesifik notelp) ) ->
            transisi DataPelangganTermuat (DataPelangganBray.init model.sesi notelp)

        ( Just _, Just (Route.DaftarTagihan nomet) ) ->
            transisi DaftarTagihanTermuat (DaftarTagihanBray.init model.sesi nomet)

        ( Just _, Just (Route.TagihanSpesifik nomet tahun bulan) ) ->
            transisi TagihanTermuat (TagihanBray.init model.sesi nomet tahun bulan)

        ( Just _, Just Route.Root ) ->
            model => Route.modifikasiUrl Route.Beranda

        ( Just _, Just Route.Keluar ) ->
            let
                ses =
                    model.sesi
            in
            { model | sesi = { ses | pengguna = Nothing } }
                => Cmd.batch
                    [ Ports.storeSession Nothing
                    , Route.modifikasiUrl Route.Masuk
                    ]

        ( Nothing, Just Route.Masuk ) ->
            { model | statusLaman = Termuat (Masuk Masuk.modelAwal) } => Cmd.none

        ( Just _, Just Route.Masuk ) ->
            model
                => Cmd.batch
                    [ Route.modifikasiUrl Route.Beranda
                    ]

        ( _, _ ) ->
            model
                => Cmd.batch
                    [ Ports.storeSession Nothing
                    , Route.modifikasiUrl Route.Masuk
                    ]


lamanJadiGalat : Model -> LamanAktif -> String -> ( Model, Cmd Msg )
lamanJadiGalat model lamanaktif pesangalat =
    let
        galat =
            GagalMuat.lamanGagalDimuat lamanaktif pesangalat
    in
    { model | statusLaman = Termuat (GagalDimuat galat) } => Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    updateLaman (getLaman model.statusLaman) msg model


updateLaman : HalamanBrowser -> Msg -> Model -> ( Model, Cmd Msg )
updateLaman laman msg model =
    let
        sesi =
            model.sesi

        toLaman tomodel tomsg subupdate submsg submodel =
            let
                ( newmodel, newcmd ) =
                    subupdate submsg submodel
            in
            ( { model | statusLaman = Termuat (tomodel newmodel) }, Cmd.map tomsg newcmd )

        galat =
            lamanJadiGalat model
    in
    case ( msg, laman ) of
        ( SetRoute r, _ ) ->
            setRute r model

        ( BerandaTermuat (Ok submodel), _ ) ->
            { model | statusLaman = Termuat (Beranda submodel) } => Cmd.none

        ( BerandaTermuat (Err galat), _ ) ->
            { model | statusLaman = Termuat (GagalDimuat galat) } => Cmd.none

        ( DaftarDataPelangganTermuat (Ok submodel), _ ) ->
            { model | statusLaman = Termuat (LihatDaftarPelanggan submodel) } => Cmd.none

        ( DaftarDataPelangganTermuat (Err galat), _ ) ->
            { model | statusLaman = Termuat (GagalDimuat galat) } => Cmd.none

        ( DataPelangganTermuat (Ok submodel), _ ) ->
            { model | statusLaman = Termuat (LihatPelanggan submodel) } => Cmd.none

        ( TagihanTermuat (Ok submodel), _ ) ->
            { model | statusLaman = Termuat (LihatTagihan submodel) } => Cmd.none

        ( TagihanTermuat (Err galat), _ ) ->
            { model | statusLaman = Termuat (GagalDimuat galat) } => Cmd.none

        ( DaftarTagihanTermuat (Ok submodel), _ ) ->
            { model | statusLaman = Termuat (LihatDaftarTagihan submodel) } => Cmd.none

        ( DaftarTagihanTermuat (Err galat), _ ) ->
            { model | statusLaman = Termuat (GagalDimuat galat) } => Cmd.none

        ( MasukMsg submsg, Masuk submodel ) ->
            let
                ( ( modellaman, cmd ), msgdarilaman ) =
                    Masuk.update model.sesi submsg submodel

                newmodel =
                    case msgdarilaman of
                        Masuk.NoOp ->
                            model

                        Masuk.SetPengguna pengguna ->
                            { model | sesi = { pengguna = Just pengguna } }
            in
            { newmodel | statusLaman = Termuat (Masuk modellaman) } => Cmd.map MasukMsg cmd

        ( SetPengguna p, _ ) ->
            let
                cmd =
                    if sesi.pengguna /= Nothing && p == Nothing then
                        Route.modifikasiUrl Route.Masuk
                    else
                        Cmd.none
            in
            { model | sesi = { sesi | pengguna = p } } => cmd

        ( _, Kosong ) ->
            model => Cmd.none

        ( _, _ ) ->
            model => Cmd.none


main : Program Value Model Msg
main =
    Navigation.programWithFlags (Route.dariLokasi >> SetRoute)
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
