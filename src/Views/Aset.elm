module Views.Aset exposing (..)

import Html exposing (Attribute, Html)
import Html.Attributes as Attr


type Gambar
    = Gambar String


error : Gambar
error =
    Gambar "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Jambu_air.jpg/320px-Jambu_air.jpg"


src : Gambar -> Attribute msg
src (Gambar u) =
    Attr.src u
