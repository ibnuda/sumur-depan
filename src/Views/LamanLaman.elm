module Views.LamanLaman exposing (..)

import Data.Pengguna exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Lazy exposing (..)
import Route exposing (Rute, ruteKeString)
import Util exposing ((=>))
import Views.Spinner exposing (spinner)


type LamanAktif
    = Lain
    | Masuk
    | Beranda
    | AktifDaftarPelanggan
    | AktifPelangganSpesifik
    | AktifDaftarTagihan
    | AktifTagihanSpesifik


bingkaix : Bool -> Maybe Pengguna -> LamanAktif -> Html msg -> Html msg
bingkaix memuatkah pengguna laman konten =
    div []
        [ konten
        ]


bingkai : Bool -> Maybe Pengguna -> LamanAktif -> Html msg -> Html msg
bingkai _ mpengguna _ konten =
    case mpengguna of
        Nothing ->
            viewKosong konten

        Just _ ->
            viewKonten konten


viewKosong : Html msg -> Html msg
viewKosong konten =
    div [ class "pure-g" ]
        [ div [ class "content pure-u-1 pure-u-md-24-24" ]
            [ konten
            ]
        ]


viewKonten : Html msg -> Html msg
viewKonten konten =
    div [ class "pure-g" ]
        [ div [ class "pure-u-1 pure-u-md-3-24" ]
            [ div [ id "menu" ]
                [ div [ class "pure-menu" ]
                    [ p [ class "pure-menu-heading" ]
                        [ text "Admin" ]
                    , ul [ class "pure-menu-list" ]
                        [ li [ class "menu-item-divided" ]
                            [ a [ href (ruteKeString Route.DaftarPengguna) ]
                                [ text "Daftar Pengguna" ]
                            ]
                        , li [ class "menu-item-divided" ]
                            [ a [ href (ruteKeString Route.Keluar) ]
                                [ text "Keluar" ]
                            ]
                        ]
                    ]
                ]
            ]
        , div [ class "content pure-u-1 pure-u-md-21-24" ]
            [ konten
            ]
        ]


viewHeader : LamanAktif -> Maybe Pengguna -> Bool -> Html msg
viewHeader laman pengguna memuatkah =
    nav []
        [ div []
            [ a [ Route.href Route.Beranda ]
                [ text "Sumur Dalam" ]
            , ul [] <|
                lazy2 Util.viewIf memuatkah spinner
                    :: viewSignIn laman pengguna
            ]
        ]


viewSignIn : LamanAktif -> Maybe Pengguna -> List (Html msg)
viewSignIn page user =
    let
        linkTo =
            navbarLink page
    in
    case user of
        Nothing ->
            [ linkTo Route.Masuk [ text "Masuk" ]
            ]

        Just user ->
            [ linkTo Route.DaftarPengguna [ text "Pelanggan" ]
            , linkTo Route.Beranda [ text "Beranda" ]
            , linkTo Route.Keluar [ text "Keluar" ]
            ]


navbarLink : LamanAktif -> Rute -> List (Html msg) -> Html msg
navbarLink laman rute kontentlink =
    li [ classList [ ( "nav-item", True ), ( "active", isActive laman rute ) ] ]
        [ a [ Route.href rute ] kontentlink ]


isActive : LamanAktif -> Rute -> Bool
isActive laman rute =
    case ( laman, rute ) of
        ( Beranda, Route.Beranda ) ->
            True

        ( Masuk, Route.Masuk ) ->
            True

        _ ->
            False
